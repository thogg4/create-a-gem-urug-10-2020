# URUG October 2020 - Creating a Ruby gem

* [What is a Ruby gem?](https://gitlab.com/thogg4/create-a-gem-urug-10-2020#what-is-a-ruby-gem)
* [Initial Creation](https://gitlab.com/thogg4/create-a-gem-urug-10-2020#initial-creation)
* [Configuration](https://gitlab.com/thogg4/create-a-gem-urug-10-2020#configuration)
* [Dependencies](https://gitlab.com/thogg4/create-a-gem-urug-10-2020#dependencies)
* [Testing](https://gitlab.com/thogg4/create-a-gem-urug-10-2020#testing)


### What is a Ruby gem?
https://guides.rubygems.org/what-is-a-gem/

A ruby gem is a (usually small) program that is distributed by the RubyGems library built into the ruby language.

There are a few categories of gems:
* CLI - stripe-cli
* Application enhancement - pg
* Run processes - sidekiq

### Initial Creation
```
bundle gem blamer
Creating gem 'blamer'...
MIT License enabled in config
Code of conduct enabled in config
      create  blamer/Gemfile
      create  blamer/lib/blamer.rb
      create  blamer/lib/blamer/version.rb
      create  blamer/blamer.gemspec
      create  blamer/Rakefile
      create  blamer/README.md
      create  blamer/bin/console
      create  blamer/bin/setup
      create  blamer/.gitignore
      create  blamer/.travis.yml
      create  blamer/test/test_helper.rb
      create  blamer/test/blamer_test.rb
      create  blamer/LICENSE.txt
      create  blamer/CODE_OF_CONDUCT.md
Initializing git repo in /Users/tim/projects/blamer
Gem 'blamer' was successfully created. For more information on making a RubyGem visit https://bundler.io/guides/creating_gem.html
```

##### Important files
* `blamer.gemspec` - Declares what the gem does and its dependencies
    * Changes needed in gemspec - `authors`, `email`, `summary`, `description`, `homepage`, `source_code_uri`, `changelog_uri`
* `lib/blamer.rb` - Contains gem logic

### Testing
* `test/test_helper.rb`

### Configuration
* `lib/blamer/configuration.rb`

